<?php

namespace Drupal\webform_unsubscribe\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Remove a submission if a secure token is correct.
 */
class DeleteWebformSubmissionController extends ControllerBase {

  /**
   * Settings service.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->settings = $container->get('settings');
    return $instance;
  }

  /**
   * Delete the webform submission if the secret token is correct.
   *
   * Check that passed token is matching the hashed 'submission id' + salt,
   * and delete the submission if this is true.
   *
   * @return Response
   *   Return response.
   */
  public function delete(WebformSubmission $webform_submission, $token) {

    $webform = $webform_submission->getWebform();
    $allow_delete = $webform->getThirdPartySetting('webform_unsubscribe', 'allow_delete_submission_using_secure_token');
    $message_after_deletion = $webform->getThirdPartySetting('webform_unsubscribe', 'message_after_submission_deletion');
    // To improve the security, сheck if the option 'Allow users to delete a
    // submission using a secure token' is set. And if no, then redirect a user
    // to the 'Page not found'.
    if (empty($allow_delete)) {
      return $this->redirect('system.404');
    }

    $hash_salt = $this->settings::getHashSalt();
    $correct_token = md5($webform_submission->id() . $hash_salt);

    if ($token == $correct_token) {
      $webform_submission->delete();

      return [
        '#markup' => $this->t($message_after_deletion),
      ];
    }
    // If the token is not correct then redirect a user to the 'Page not found'.
    return $this->redirect('system.404');
  }

}
