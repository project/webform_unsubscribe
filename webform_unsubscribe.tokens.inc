<?php

/**
 * @file
 * Builds placeholder replacement token for webforms submission delete URL.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Implements hook_token_info().
 *
 * Create the [webform_submission:delete-url] token.
 */
function webform_unsubscribe_token_info() {
  // We don't create a new type, but add the token to the existing
  // 'webform_submission' type, to it displays in the list of the existing
  // 'webform_submission' tokens.
  $tokens['webform_submission']['delete-url'] = [
    'name' => t('Delete (token) URL'),
    'description' => t('The URL that can used to delete the webform submission. The webform must be configured to allow users to delete a submission using a secure token.'),
    'type' => 'url',
  ];

  return ['tokens' => $tokens];
}

/**
 * Implements hook_tokens().
 */
function webform_unsubscribe_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'webform_submission') {

    foreach ($tokens as $name => $original) {
      switch ($name) {

        case 'delete-url':
          /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
          $webform_submission = $data['webform_submission'];
          $webform = $webform_submission->getWebform();
          $allow_delete = $webform->getThirdPartySetting('webform_unsubscribe', 'allow_delete_submission_using_secure_token');

          if (empty($allow_delete)) {
            return;
          }

          $id = $webform_submission->id();
          $hash_salt = Settings::getHashSalt();

          // Generate the token and build the URL for removing the submission.
          $token = md5($id . $hash_salt);

          $route_parameters = [
            'webform_submission' => $id,
            'token' => $token,
          ];

          $options = ['absolute' => TRUE];
          $url = Url::fromRoute('webform_unsubscribe.delete_webform_submission', $route_parameters, $options);

          $replacements[$original] = $url->toString();
          break;
      }
    }
  }

  return $replacements;
}
